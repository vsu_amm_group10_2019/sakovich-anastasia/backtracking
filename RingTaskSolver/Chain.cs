using System.Collections.Generic;
using System.Linq;

namespace Backtracking.RingTaskSolver
{
    public partial class Solver
    {
        private class Chain
        {
            private int _vowelsCount;
            private int _consonantCount;
            private readonly IAlphabetProvider _alphabetProvider;
            private readonly List<string> _words;

            private Chain(IEnumerable<string> words, IAlphabetProvider alphabetProvider, int vowelsCount, int consonantCount)
            {
                _words = new List<string>(words);
                _alphabetProvider = alphabetProvider;
                vowelsCount = _vowelsCount;
                consonantCount = _consonantCount;
            }

            public Chain(IAlphabetProvider alphabetProvider)
            {
                _words = new List<string>();
                _alphabetProvider = alphabetProvider;
            }

            public bool TryAddToChain(string word)
            {
                if (_words.Count < 1)
                {
                    _vowelsCount = SumVowel(word);
                    _consonantCount = SumConsonant(word);
                }
                else if (!CanChain(_words[^1], word))
                    return false;
                    
                _words.Add(word);

                return true;
            }

            public bool RemoveLast()
            {
                bool success = _words.Count > 0;
                if (success)
                    _words.RemoveAt(_words.Count - 1);
                return success;
            }

            private bool CanChain(string start, string end)
                => _vowelsCount == SumVowel(end)
                    && _consonantCount == SumConsonant(end)
                    && start[^1] == end[0];

            private int SumVowel(string word)
                => word.Where(symbol => _alphabetProvider.IsVowel(symbol)).Count();

            private int SumConsonant(string word)
                => word.Where(symbol => _alphabetProvider.IsConsonant(symbol)).Count();

            public Chain Clone()
                => new Chain(_words, _alphabetProvider, _vowelsCount, _consonantCount);

            public int LettersCount()
                => _words.Sum(selector => selector.Length);

            public override string ToString()
                => string.Join(", ", _words);

            public int CompareTo(Chain other)
                => LettersCount() < other.LettersCount()
                    ? -1
                    : (LettersCount() != other.LettersCount() ? 1 : 0);
        }
    }
}