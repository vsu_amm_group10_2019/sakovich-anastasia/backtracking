namespace Backtracking
{
    public interface IAlphabetProvider
    {
        bool IsVowel(char symbol);
        bool IsConsonant(char symbol);
    }
}