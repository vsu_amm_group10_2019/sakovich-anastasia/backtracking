namespace Backtracking
{
    public class RussianAlphabetProvider : IAlphabetProvider
    {
        private const string Vowels = "аеёиоуыэюя";
        private const string Consonants = "бвгджзйклмнпрстфхцчшщ";

        public bool IsConsonant(char symbol)
            => Consonants.Contains(char.ToLower(symbol));

        public bool IsVowel(char symbol)
            => Vowels.Contains(char.ToLower(symbol));
    }
}