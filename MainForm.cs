﻿using Backtracking.RingTaskSolver;
using System.Linq;
using System.Windows.Forms;

namespace Backtracking
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }
        private static string[] GetWords(string text)
            => text.Split(',').Select(x => x.Trim()).ToArray();

        private void EnterPressed(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && sender == outputTextBox)
                calculateButton.PerformClick();
        }

        private void CalculateClicked(object sender, System.EventArgs e)
        {
            IAlphabetProvider alphabetProvider = russianRadioButton.Checked
                ? new RussianAlphabetProvider()
                : new EnglishAlphabetProvider();

            var solver = new Solver(GetWords(inputTextBox.Text), alphabetProvider);
            (string, int) result = solver.Solve();

            outputTextBox.Text = $"Искомое решение: { result.Item1 } ({ result.Item2 })";
        }
    }
}
