using System.Collections.Generic;
using System.Linq;

namespace Backtracking.RingTaskSolver
{
    public partial class Solver
    {
        private Chain _solve;

        private readonly string[] _words;
        private readonly bool[] _used;
        private readonly Chain _currentChain;

        public Solver(IEnumerable<string> words, IAlphabetProvider alphabetProvider)
        {
            _words = words.ToArray();
            _used = new bool[_words.Length];
            _currentChain = new Chain(alphabetProvider);
        }
        
        private void SolveRecursive()
        {
            bool allUsed = true;

            for (int i = 0; i < _words.Length; ++i)
            {
                if (_used[i] || !_currentChain.TryAddToChain(_words[i]))
                    continue;
                allUsed = false;
                _used[i] = true;
                SolveRecursive();
                _currentChain.RemoveLast();
                _used[i] = false;
            }

            if (allUsed && (_solve is null || _solve.CompareTo(_currentChain) < 0))
                _solve = _currentChain.Clone();
        }

        public (string, int) Solve()
        {
            if (_solve is null)
                SolveRecursive();
            return (_solve.ToString(), _solve.LettersCount());
        }
    }
}