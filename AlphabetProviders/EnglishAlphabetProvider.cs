namespace Backtracking
{
    public class EnglishAlphabetProvider : IAlphabetProvider
    {
        private const string Vowels = "aeiouy";
        private const string Consonants = "bcdfghjklmnpqrstvwxyz";

        public bool IsConsonant(char symbol)
            => Consonants.Contains(char.ToLower(symbol));

        public bool IsVowel(char symbol)
            => Vowels.Contains(char.ToLower(symbol));
    }
}